﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWebApp.Models
{
    public class Newsdetail
    {
        public int ID { get; set; }
   
        [Required(ErrorMessage = "Заполняйте все поля правильно!")]
        public string Name { get; set; }
        [Required]
        public string Language { get; set; }
        [Required]
        public string Category { get; set; }

        
        public DateTime Date { get; set; }

        [Required]
        public string Summary { get; set; }
        [Required]
        public string FullContent { get; set; }
      
        public byte[] PhotoForPreview { get; set; }

        public byte[] PhotoForFulltext { get; set; }
    }
}
