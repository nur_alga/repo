#pragma checksum "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "36f89cf80748dd06ee4605659f00168abc1ae6a2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Policy), @"mvc.1.0.view", @"/Views/Home/Policy.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Policy.cshtml", typeof(AspNetCore.Views_Home_Policy))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\_ViewImports.cshtml"
using NewsWebApp;

#line default
#line hidden
#line 2 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\_ViewImports.cshtml"
using NewsWebApp.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"36f89cf80748dd06ee4605659f00168abc1ae6a2", @"/Views/Home/Policy.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"956076fa6400e2214c314d926d1c2b6aa883a1af", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Policy : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<NewsWebApp.Models.Newsdetail>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/bootstrap/css/bootstrap.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("text-decoration: none; color:black;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("text-decoration: none;  color:black;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("text-decoration: none; width: 150px; display: block;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("text-decoration: none;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
  
    ViewData["Title"] = "Политика";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
            BeginContext(141, 35, true);
            WriteLiteral("<!DOCTYPE html>\r\n<html lang=\"en\">\r\n");
            EndContext();
            BeginContext(176, 143, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e0289679d6404edfb5c39dd6892bc630", async() => {
                BeginContext(182, 34, true);
                WriteLiteral("\r\n    <meta charset=\"utf-8\">\r\n    ");
                EndContext();
                BeginContext(216, 66, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "04191b6304914cf3bc00a329300f6322", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(282, 30, true);
                WriteLiteral("\r\n    <title>Новости</title>\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(319, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(321, 2950, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9bf35584ad3240689965b11b583f32d3", async() => {
                BeginContext(327, 33, true);
                WriteLiteral("\r\n    <div class=\"container\">\r\n\r\n");
                EndContext();
#line 16 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
         foreach (var news in Model.OrderByDescending(item => item.Date))
        {
            if (news.Category.Equals("Политика"))
            {
                

#line default
#line hidden
                BeginContext(530, 154, true);
                WriteLiteral("                <div class=\"shadow-lg p-3 mb-5 bg-white rounded\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-4\">\r\n");
                EndContext();
#line 24 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
                              
                                var base64 = Convert.ToBase64String(news.PhotoForPreview);
                                var imgsrc = string.Format("data:image/gif;base64,{0}", base64);

                            

#line default
#line hidden
                BeginContext(939, 28, true);
                WriteLiteral("                            ");
                EndContext();
                BeginContext(967, 139, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "92c6b2fd97ed4344a5fdafb34e6d55b9", async() => {
                    BeginContext(1001, 42, true);
                    WriteLiteral("<img class=\"d-flex align-self-center mr-3\"");
                    EndContext();
                    BeginWriteAttribute("src", " src=\'", 1043, "\'", 1056, 1);
#line 29 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
WriteAttributeValue("", 1049, imgsrc, 1049, 7, false);

#line default
#line hidden
                    EndWriteAttribute();
                    BeginContext(1057, 45, true);
                    WriteLiteral(" style=\"max-width:300px; max-height:300px\" />");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "href", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 976, "~/Home/Content/", 976, 15, true);
#line 29 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
AddHtmlAttributeValue("", 991, news.ID, 991, 8, false);

#line default
#line hidden
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1106, 164, true);
                WriteLiteral("\r\n                        </div>\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"col-sm\">\r\n                                ");
                EndContext();
                BeginContext(1270, 102, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "06f90e8f9b5e432bb367db8951a47f6e", async() => {
                    BeginContext(1348, 5, true);
                    WriteLiteral(" <h4>");
                    EndContext();
                    BeginContext(1354, 9, false);
#line 33 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
                                                                                                              Write(news.Name);

#line default
#line hidden
                    EndContext();
                    BeginContext(1363, 5, true);
                    WriteLiteral("</h4>");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "href", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 1323, "~/Home/Content/", 1323, 15, true);
#line 33 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
AddHtmlAttributeValue("", 1338, news.ID, 1338, 8, false);

#line default
#line hidden
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1372, 120, true);
                WriteLiteral("\r\n                            </div>\r\n                            <div class=\"col-sm\">\r\n                                ");
                EndContext();
                BeginContext(1492, 96, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "383117998cad4e54a5729ae4c94bd62d", async() => {
                    BeginContext(1572, 12, false);
#line 36 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
                                                                                                          Write(news.Summary);

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "href", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 1546, "~/Home/Content/", 1546, 15, true);
#line 36 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
AddHtmlAttributeValue("", 1561, news.ID, 1561, 8, false);

#line default
#line hidden
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1588, 187, true);
                WriteLiteral("\r\n                            </div>\r\n                            <hr>\r\n                            <div class=\"container\">\r\n                                <div class=\"row no-gutters\">\r\n");
                EndContext();
#line 41 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
                                     if (User.Identity.IsAuthenticated)
                                    {

#line default
#line hidden
                BeginContext(1887, 108, true);
                WriteLiteral("                                        <div class=\"nav-item\">\r\n                                            ");
                EndContext();
                BeginContext(1995, 142, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4f59994aa3284b3b984a053202d391e4", async() => {
                    BeginContext(2087, 46, true);
                    WriteLiteral("Изменить<span class=\"sr-only\">(current)</span>");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "href", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 2065, "~/News/Edit/", 2065, 12, true);
#line 44 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
AddHtmlAttributeValue("", 2077, news.ID, 2077, 8, false);

#line default
#line hidden
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2137, 50, true);
                WriteLiteral("\r\n                                        </div>\r\n");
                EndContext();
                BeginContext(2189, 108, true);
                WriteLiteral("                                        <div class=\"nav-item\">\r\n                                            ");
                EndContext();
                BeginContext(2297, 143, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4bbada55af7c441794e28af7b6cc0246", async() => {
                    BeginContext(2391, 45, true);
                    WriteLiteral("Удалить<span class=\"sr-only\">(current)</span>");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "href", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 2367, "~/News/Delete/", 2367, 14, true);
#line 48 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
AddHtmlAttributeValue("", 2381, news.ID, 2381, 8, false);

#line default
#line hidden
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2440, 50, true);
                WriteLiteral("\r\n                                        </div>\r\n");
                EndContext();
#line 50 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
                                    }
                                    else
                                    {

#line default
#line hidden
                BeginContext(2610, 124, true);
                WriteLiteral("                                        <div class=\"col-12 col-sm-6 col-md-8\">\r\n                                            ");
                EndContext();
                BeginContext(2734, 117, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "063d59f760d145859ed2298ca9e0c768", async() => {
                    BeginContext(2799, 48, true);
                    WriteLiteral("Подробнее <span class=\"sr-only\">(current)</span>");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "href", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 2774, "~/News/Details/", 2774, 15, true);
#line 54 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
AddHtmlAttributeValue("", 2789, news.ID, 2789, 8, false);

#line default
#line hidden
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2851, 50, true);
                WriteLiteral("\r\n                                        </div>\r\n");
                EndContext();
#line 56 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
                                    }

#line default
#line hidden
                BeginContext(2940, 88, true);
                WriteLiteral("                                    <div class=\"col-6 col-md-4\" style=\"float:right\"> <p>");
                EndContext();
                BeginContext(3029, 23, false);
#line 57 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
                                                                                   Write(news.Date.ToString("d"));

#line default
#line hidden
                EndContext();
                BeginContext(3052, 172, true);
                WriteLiteral("</p></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n");
                EndContext();
#line 63 "C:\Users\Админ\source\repos\repo\NewsWebApp\Views\Home\Policy.cshtml"
            }
        }

#line default
#line hidden
                BeginContext(3250, 14, true);
                WriteLiteral("    </div>\r\n\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3271, 11, true);
            WriteLiteral("\r\n</html>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<NewsWebApp.Models.Newsdetail>> Html { get; private set; }
    }
}
#pragma warning restore 1591
