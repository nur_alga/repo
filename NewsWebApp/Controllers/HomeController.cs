﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NewsWebApp.Models;

namespace NewsWebApp.Controllers
{
    public class HomeController : Controller
    {
        private NewsContext _context;
        public HomeController(NewsContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult Index()
        {
            var item = _context.Newsdetails.ToList();
            return View(item);
        }
        

        public IActionResult Content(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var news = _context.Newsdetails.FirstOrDefault(m => m.ID == id);
            if (news == null)
            {
                return NotFound();
            }
            return View(news);
        }

        public ActionResult Policy()
        {
            var item = _context.Newsdetails.ToList();
            return View(item);
        }


        public ActionResult Buisiness()
        {
            var item = _context.Newsdetails.ToList();
            return View(item);
        }

        public ActionResult ShowBuisiness()
        {
            var item = _context.Newsdetails.ToList();
            return View(item);
        }

        [HttpGet]
        public ActionResult Soc()
        {
            var item = _context.Newsdetails.ToList();
            return View(item);
        }

       


    }
}
