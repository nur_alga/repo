﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NewsWebApp.Models;

namespace NewsWebApp.Controllers
{
    public class NewsController : Controller
    {
        private readonly NewsContext _context;

        public NewsController(NewsContext context)
        {
            _context = context;
        }

        // GET: News
        public async Task<IActionResult> Index()
        {
            return View(await _context.Newsdetails.ToListAsync());
        }
        // GET: News/Create
        
        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Create(Newsdetail news, List<IFormFile> PhotoForPreview, List<IFormFile> PhotoForFulltext)
        {
            if (ModelState.IsValid)
            {
                Newsdetail ns = await _context.Newsdetails
                    .FirstOrDefaultAsync(u => u.Name == news.Name && u.Summary == news.Summary && u.PhotoForFulltext == news.PhotoForFulltext);
                if (ns == null)
                {
                     ModelState.AddModelError("Name", "Заполняйте все поля правильно!");
                    ModelState.AddModelError("Date", "Дата не устоновлен!");
                }
                else
                {
                    foreach (var item in PhotoForPreview)
                    {
                        if (item.Length > 0)
                        {
                            using (var stream = new MemoryStream())
                            {
                                await item.CopyToAsync(stream);
                                news.PhotoForPreview = stream.ToArray();
                            }
                        }

                    }
                    foreach (var item in PhotoForFulltext)
                    {
                        if (item.Length > 0)
                        {
                            using (var stream = new MemoryStream())
                            {
                                await item.CopyToAsync(stream);
                                news.PhotoForFulltext = stream.ToArray();
                            }
                        }
                    }
                    _context.Newsdetails.Add(news);
                    _context.SaveChanges();
                    ModelState.Clear();
                }
            }
           
            return View();
        }

        // GET: News/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsdetail = await _context.Newsdetails.FindAsync(id);
            if (newsdetail == null)
            {
                return NotFound();
            }
            return View(newsdetail);
        }

        // POST: News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, List<IFormFile> PhotoForPreview, List<IFormFile> PhotoForFulltext, [Bind("ID,Name,Language,Category,Date,Summary,FullContent")] Newsdetail newsdetail)
        {
            if (id != newsdetail.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                
                if (PhotoForPreview == null)
                {
                    ModelState.AddModelError("ModelOnly", "Заполняйте все поля правильно!");
                    
                }
                else
                {
                    foreach (var item in PhotoForPreview)
                    {
                        if (item.Length > 0)
                        {
                            using (var stream = new MemoryStream())
                            {
                                await item.CopyToAsync(stream);
                                newsdetail.PhotoForPreview = stream.ToArray();
                            }
                        }

                    }
                    foreach (var item in PhotoForFulltext)
                    {
                        if (item.Length > 0)
                        {
                            using (var stream = new MemoryStream())
                            {
                                await item.CopyToAsync(stream);
                                newsdetail.PhotoForFulltext = stream.ToArray();
                            }
                        }
                    }
                    _context.Update(newsdetail);
                    await _context.SaveChangesAsync();
                }
                return RedirectToAction(nameof(Index));
            }
            return View(newsdetail);
        }

        // GET: News/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsdetail = await _context.Newsdetails
                .FirstOrDefaultAsync(m => m.ID == id);
            if (newsdetail == null)
            {
                return NotFound();
            }

            return View(newsdetail);
        }

        // POST: News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var newsdetail = await _context.Newsdetails.FindAsync(id);
            _context.Newsdetails.Remove(newsdetail);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NewsdetailExists(int id)
        {
            return _context.Newsdetails.Any(e => e.ID == id);
        }
    }
}
